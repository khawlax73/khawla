import cv2
import numpy as np

cap = cv2.VideoCapture(0)

while(1):
    _, frame = cap.read()
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    
    lower_red = np.array([30,150,50])
    upper_red = np.array([255,255,180])
    
    
    lower_blue = np.array([50,50,50])
    upper_blue = np.array([130,255,255])
    
    lower_green = np.array([0, 0, 0 ])
    upper_green = np.array([180,255,255])
    
    mask1 = cv2.inRange(hsv, lower_red, upper_red)
    res1 = cv2.bitwise_and(frame,frame, mask= mask1)
    
    mask2 = cv2.inRange(hsv, lower_blue, upper_blue)
    res2 = cv2.bitwise_and(frame,frame, mask= mask2)
    
    mask3 = cv2.inRange(hsv, lower_green, upper_green)
    res3 = cv2.bitwise_and(frame,frame, mask= mask3)
    
    res= cv2.add(res1,res2,res3)

    cv2.imshow('frame',frame)
   
    cv2.imshow('res',res)
    
    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break

cv2.destroyAllWindows()
cap.release()